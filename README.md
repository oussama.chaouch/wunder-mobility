# Wunder Mobility

Welcome to the My TypeScript Project repository. This document will guide you through the setup and usage of the project.

## Prerequisites

Before you can run this project, you need to have the following software installed:

1. **Node.js**: Ensure that you have Node.js installed on your machine. You can download and install it from the [official Node.js website](https://nodejs.org/).
2. **TypeScript**: Install TypeScript globally using npm. You can do this by running the following command:
   ```sh
   npm install -g typescript@latest
   ```

## Dependencies

This project relies on specific versions of Node.js and TypeScript to function correctly. Please make sure you have the following versions installed:

- **Node.js**: v20.15.0 (or any compatible version)
- **TypeScript**: v5.5.3 (or the latest version available)

## Project Setup

Follow these steps to set up and run the project:

1. Clone the repository:
   ```sh
   git clone https://github.com/your-username/your-repo.git
   cd your-repo
   ```

2. Install project dependencies:
   ```sh
   npm install
   ```

3. Compile the TypeScript code:
   ```sh
   tsc
   ```

4. Run the project:
   ```sh
   node dist/main.js
   ```

## Commands

Here is a list of useful commands to help you manage and execute the project:

- **Install dependencies**:
  ```sh
  npm install
  ```

- **Compile TypeScript**:
  ```sh
  tsc
  ```

- **Run the project**:
  ```sh
  node dist/main.js
  ```

- **Clean the build directory**:
  ```sh
  rm -rf dist
  ```

- **Run tests**:
  ```sh
  npm test
  ```

