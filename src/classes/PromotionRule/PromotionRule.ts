import { Coupon } from "../../interfaces/Coupon";
import { DiscountPromotion } from "../../interfaces/DiscountPromotion";
import { Purchase } from "../../interfaces/Purchase";
import { ReductionPromotion } from "../../interfaces/ReductionPromotion";
import { Promotion } from "../../types/Promotion";

export class PromotionRule {
    private promotions: Promotion[] = [];

    // ADD promotions to the list of promotions
    public addPromotion(promotion: Promotion) {
      this.promotions.push(promotion);
      console.log("Promotion added successfully")
    }
  
    // Display the list of promotions
    public getPromotions(): Promotion[] {
      return this.promotions;
    }

    // Apply added promotion and calculation of the new total price after promotion 
    public applyPromotions = (purchases: Purchase[], total: number): number => {
        let price = total;
        purchases.forEach((purchase,index) => {
            this.promotions.forEach(promotion => {
                switch (promotion.type) {
                    case 'reduction':
                        // in the case of reduction promotion added
                        const reductionPromo = promotion as ReductionPromotion;
                        if(
                            purchase.productId === reductionPromo.productId &&
                            purchase.quantity >= reductionPromo.quantity
                        ){
                            price -= reductionPromo.discountAmount * purchase.quantity;
                        }
                        break;
                    case 'discount':
                        // in the case of discount promotion added
                        const discountPromo = promotion as DiscountPromotion
                        if(price > discountPromo.amount && index + 1 === purchases.length){
                            const discountAmount = price * discountPromo.percentage / 100;
                            price = +(price - discountAmount).toFixed(2);
                        }
                        break;
                    case "coupon":
                        // in the case of coupon promotion added
                        const couponPromo = promotion as Coupon;
                        if (purchase.productId === couponPromo.code) {
                            price *= 1 - couponPromo.discount / 100;
                        }
                        break;
                }
            });
        });
        return price;
    }
}