import { Product } from "../../interfaces/Product";
import { Purchase } from "../../interfaces/Purchase";
import { PromotionRule } from "../PromotionRule/PromotionRule";


export class Checkout {

    private promotionRules: PromotionRule;
    private total: number = 0;
    private purchases: Purchase[] = [];
    // fill products catalog
    private products: Product[] = [
      {
        id: "001",
        name: "Curry Sauce",
        description: "A rich and spicy curry sauce made from authentic ingredients.",
        price: 1.95
      },
      {
        id: "002",
        name: "Pizza",
        description: "A delicious pizza with a crispy crust, topped with fresh ingredients.",
        price: 5.99
      },
      {
        id: "003",
        name: "Men's T-shirt",
        description: "A comfortable and stylish men's T-shirt made from high-quality cotton.",
        price: 25.00
      }
  ];

    constructor(promotionRules: PromotionRule){
        this.promotionRules = promotionRules;
    }

    // Add product to list of purchase
    public scan = (productId: string): void => {
        const itemExists = this.products.filter((element) => element.id === productId )[0];
        if(itemExists){
            this.addPurchases(productId);
            this.total += itemExists.price;
        }else {
          console.log('this item does not exists in our system');
        }
    }

    // Add product function
    public addProduct = (product: Product): void => {
      this.products.push(product);
    }
    
    public getProducts = (): Product[] => {
      return this.products;
    }

    // Add purchase function
    public addPurchases = (productId: string) => {
      let index = -1;
      this.purchases.map((element,idx) => {
        if(element.productId === productId){
          index = idx;
        }
      });
      if(index !== -1){
        this.purchases[index].quantity += 1;
      }else{
        this.purchases.push({productId,quantity: 1});
      }
      console.log('Purchase added successfully')
    }

    public getPurchaseList = (): Purchase[] => {
      return this.purchases;
    }

    // Calculate purchases total price after applying promotion
    public getTotalPrice = (): number => {
      this.total = this.promotionRules.applyPromotions(this.purchases,this.total);
      return this.total;
    }
}