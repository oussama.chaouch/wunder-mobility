import { Coupon } from "../interfaces/Coupon";
import { DiscountPromotion } from "../interfaces/DiscountPromotion";
import { ReductionPromotion } from "../interfaces/ReductionPromotion";

export type Promotion =
  | ReductionPromotion
  | DiscountPromotion
  | Coupon
