export interface DiscountPromotion {
    type: "discount";
    description: string;
    percentage: number;
    amount: number;
}