export interface Coupon {
    type: "coupon";
    description: string;
    code: string;
    discount: number;
}