export interface ReductionPromotion {
    type: "reduction";
    productId: string;
    description: string;
    quantity: number;
    discountAmount: number;
}