export interface Purchase {
    productId: string;
    quantity: number;
}