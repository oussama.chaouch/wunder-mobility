import { Checkout } from "./src/classes/Ckeckout/Checkout";
import { PromotionRule } from "./src/classes/PromotionRule/PromotionRule";
import { DiscountPromotion } from "./src/interfaces/DiscountPromotion";
import { ReductionPromotion } from "./src/interfaces/ReductionPromotion";

/**
 * Create some promotions
 */
console.log('creating the list of promotions...')
const discountPromo: DiscountPromotion = {
    type: 'discount',
    description: 'If you spend over €30, you get 10% off your purchase.',
    percentage: 10,
    amount: 30
};

const reductionPromo: ReductionPromotion = {
    type: 'reduction',
    productId: '002',
    description: 'If you buy 2 or more pizzas, the price for each drops to €3.99.',
    quantity: 2,
    discountAmount: 2,
}

/**
 * Create promotions using promotionRule
 */

const promotionRules = new PromotionRule();

promotionRules.addPromotion(discountPromo);
promotionRules.addPromotion(reductionPromo);

// Create checkout object to start the purchase action
const co = new Checkout(promotionRules);

// Scan products by product id
console.log('Scaning object...')
co.scan('002');
co.scan('001');
co.scan('002');
co.scan('003');

// get the total price
console.log('The total price is: ',co.getTotalPrice());