import { PromotionRule } from "../src/classes/PromotionRule/PromotionRule";
import { DiscountPromotion } from "../src/interfaces/DiscountPromotion";
import { Product } from "../src/interfaces/Product";
import { Purchase } from "../src/interfaces/Purchase";
import { ReductionPromotion } from "../src/interfaces/ReductionPromotion";

describe('PromotionRule unit test', () => {
    let pr: PromotionRule
    let products: Product[];
    let purchases: Purchase[];
    let total: number;

    beforeEach(() => {
        pr = new PromotionRule();
        total= 0;
        products = [
          { id: '001', name: 'Curry Sauce', description: 'curry sauce description', price: 5.99 },
          { id: '002', name: 'Pizza', description: 'pizza description', price: 12.99 },
          { id: '003', name: 'Men\'s T-shirt', description: 'man tshirt description', price: 19.99 },
        ];
        purchases = [
          { productId: '001', quantity: 3 },
          { productId: '002', quantity: 2 },
        ];
        purchases.forEach(purchase => {
            const product = products.find(p => p.id === purchase.productId);
            total += product ? product.price * purchase.quantity : 0;
        });
    });
    
    test('should apply discount promotion correctly', () => {
        const discountPromo: DiscountPromotion = {
            type: 'discount',
            description: '10% off on all items',
            percentage: 10,
            amount: 30,
        };
    
        pr.addPromotion(discountPromo);
    
        const result = pr.applyPromotions(purchases, total);
        expect(result).toBeCloseTo(39.56, 2); // (5.99*3 + 12.99*2) - ( (5.99*3 + 12.99*2) * 10 / 100) = 39.555, rounded to 39.56
    });

    test('should apply reduction promotion correctly', () => {
        const reductionPromo: ReductionPromotion = {
            type: 'reduction',
            productId: '002',
            description: 'If you buy 2 or more pizzas, the price for each drops to €7.99.',
            quantity: 2,
            discountAmount: 5,
        };
    
        pr.addPromotion(reductionPromo);
    
        const result = pr.applyPromotions(purchases, total);
        expect(result).toBeCloseTo(33.95, 2); // (5.99*3 + 12.99*2) - (5*2) = 33.95,
    });
})