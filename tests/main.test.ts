import { Checkout } from '../src/classes/Ckeckout/Checkout';
import { PromotionRule } from '../src/classes/PromotionRule/PromotionRule';
import { DiscountPromotion } from '../src/interfaces/DiscountPromotion';
import { ReductionPromotion } from '../src/interfaces/ReductionPromotion';

describe('unit test', () => {
    let discountPromo: DiscountPromotion;
    let reductionPromo: ReductionPromotion;
    let pr: PromotionRule;
    let co: Checkout;

    beforeEach(() => {
        pr = new PromotionRule();
        discountPromo = {
            type: 'discount',
            description: 'spend more than €30 to get 10% off ',
            percentage: 10,
            amount: 30
        };

        reductionPromo = {
            type: 'reduction',
            productId: '002',
            description: 'buy 2 pizzas and get them for €3.99 each.',
            quantity: 2,
            discountAmount: 2,
        };
    });

    test('discountPromo should not be null or undefined and must be of type discount',() => {
        expect(discountPromo).not.toBeNull();
        expect(discountPromo).not.toBeUndefined();
        expect(discountPromo.type).toBe('discount');
    });

    test('reductionPromo should not be null or undefined and must be of type reduction', () => {
        expect(reductionPromo).not.toBeNull();
        expect(reductionPromo).not.toBeUndefined();
        expect(reductionPromo.type).toBe('reduction');
        expect(reductionPromo.productId).toBe('002');
    });

    test('pr object should be instance of PromotionRule', () => {
        expect(pr).toBeInstanceOf(PromotionRule);
    });

    /*test('', () => {

    });*/

    test('should apply promotions correctly and get the total price', () => {
        pr.addPromotion(discountPromo);
        pr.addPromotion(reductionPromo);
        co = new Checkout(pr);
        co.scan('002');
        co.scan('001');
        co.scan('002');
        co.scan('003');
        /**
         * (3.99 * 2 (because we have 2 or more pizzas) + 1.95 + 25.00) 
         * - 3.4930000000000003 (10% discount of the €30 more purchases)
         * = 31.436999999999998 
         *  rounded to 31.44
         */
        expect(co.getTotalPrice()).toBeCloseTo(31.44, 2);
      });
});