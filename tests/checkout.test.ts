import { Checkout } from "../src/classes/Ckeckout/Checkout";
import { PromotionRule } from "../src/classes/PromotionRule/PromotionRule";
import { DiscountPromotion } from "../src/interfaces/DiscountPromotion";
import { Product } from "../src/interfaces/Product";
import { Purchase } from "../src/interfaces/Purchase";

describe('', () => {
    let pr: PromotionRule;
    let products: Product[];
    let purchases: Purchase[];
    let oc: Checkout;

    beforeEach(() => {
        pr = new PromotionRule();
        const discountPromo: DiscountPromotion = {
            type: 'discount',
            description: '10% off on all items',
            percentage: 10,
            amount: 30,
        };
        pr.addPromotion(discountPromo);
        oc = new Checkout(pr);
        products = [
          { id: '001', name: 'Curry Sauce', description: 'curry sauce description', price: 5.99 },
          { id: '002', name: 'Pizza', description: 'pizza description', price: 12.99 },
          { id: '003', name: 'Men\'s T-shirt', description: 'man tshirt description', price: 19.99 },
        ];
        purchases = [];

        products.forEach(product => {
            oc.addProduct(product);
        });
    });

    test('should add product sucessfully', () => {
        oc.addProduct({
            id: '004',
            name: "LED Desk Lamp",
            description: "A sleek LED desk lamp with adjustable brightness and color temperature.",
            price: 59.99
        });

        // the result should be grater than the default list of products
        expect(oc.getProducts().length).toBeGreaterThan(products.length);
    });

    test('should scan item sucessfully', () => {
        let productId = '003';
        oc.scan(productId);
        const result = oc.getPurchaseList().find(p => p.productId === productId)
        expect(result).not.toBeUndefined();
        expect(result?.productId).toBe(productId);
    });
});
